class Credit
  include MongoMapper::Document

  key :identification, String
  key :birthday, DateTime
  key :amount, Integer
  key :months, Integer
  key :url, String
  key :credit_line_id, ObjectId
  key :credit_line_name, String
  key :credit_line_anual_rate, Float
  key :state, String
  key :risk, Float
  key :total_payment, Float
  key :total_interest, Float
  key :created_at, DateTime
  key :timestamp, Integer
  key :payments, Array

  validates :identification, presence: true, length: { in: 1..50 }
  validates :birthday, presence: true
  validates :amount, presence: true, numericality: {greater_than: 0}
  validates :months, presence: true, numericality: {greater_than: 0, integer: true}
  validates :url, presence: true
  validates :credit_line_id, presence: true

  before_create do |obj|
    obj.state = 'pending'
    obj.created_at=DateTime.now
    obj.timestamp=10.gigabyte-Time.now.to_f
    obj.credit_line_name = credit_line.name
    obj.credit_line_anual_rate = credit_line.anual_rate
  end
  after_create :queue

  def queue
    CreditQueue.send_credit(self)
  end

  def credit_line
    @credit_line||=CreditLine.find(self.credit_line_id)
  end

  def generate_payment_plan
    self.risk= self.calculate_risk
    self.create_payments
    self.state = 'done'
    self.save!    
  end

  def create_payments
    self.payments=[]
    monthly_payment = self.get_monthly_payment
    self.total_payment= monthly_payment*self.months
    self.total_interest = self.total_payment-self.amount
    balance = self.amount
    self.months.times do |index|
      self.payments<<{}.tap do |hash|
        hash[:number] = index+1
        hash[:payment]=monthly_payment
        hash[:interest]= interest = balance * self.monthly_rate
        hash[:balance] = balance = balance + interest - monthly_payment
        hash[:amortization] =  monthly_payment - interest
      end
    end
  end

  def get_monthly_payment
    @get_monthly_payment||=(self.amount / ((1-(1+self.monthly_rate)**(self.months*-1.0))/self.monthly_rate))
  end
  
  #Tasa efectiva mensual
  def monthly_rate
    @monthly_rate ||= ((1.0+(self.credit_line_anual_rate.to_f/100.0))**(1.0/self.months.to_i))-1.0
  end

  def calculate_risk
    time = Time.now+25.seconds
    index=0
    while time > Time.now do 
      SecureRandom.base64(10000000)[0..10]
      index+=1
    end
    Credit.logger.info("Executed #{index}")
    (Random.rand*11).to_i
  end
end
