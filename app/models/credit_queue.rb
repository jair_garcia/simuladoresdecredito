class CreditQueue 

  def self.get_client
    @@ironmq ||= IronMQ::Client.new()
  end

  def self.get_queue
    @@queue ||= self.get_client.queue(Settings.queue.name)
  end

  def self.send_credit(credit)
    self.get_queue.post(credit.id)
    ::NewRelic::Agent.increment_metric('custom/queue/sent')
  end
  #Control errores 
  def self.process_credits()
    Credit.logger.error("Starting")
    loop do
      puts("Procesando ...")
      Credit.logger.info("Scanning")
      begin
        message = self.get_queue.get(timeout: 30, wait: 30)
        credit = Credit.find(message.body)
        credit.generate_payment_plan
        message.delete
        ::NewRelic::Agent.increment_metric('custom/queue/done')
      rescue  
        Credit.logger.error($!.message)
      end
    end
  end
end