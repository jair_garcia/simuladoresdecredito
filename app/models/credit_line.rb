class CreditLine
  include MongoMapper::Document

  key :name, String
  key :anual_rate, Float
  key :url, String
  key :state, String

  validates :name, :length=> {maximum: 50}, :presence=>true
  validates :anual_rate, presence: true, 
  	numericality: {greater_than: 0, less_than: 100}
  validates :url, presence: true

  before_create :set_state

  def set_state
    self.state = 'active'
  end

  def company
    @company||=Company.where(url: self.url).first
  end
end
