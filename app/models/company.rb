class Company
  include MongoMapper::Document
  include BCrypt


  key :name, String
  key :url, String
  key :admin_first_name, String
  key :admin_last_name, String
  key :email, String
  key :encrypted_password, String
  key :created_at, DateTime

  validate :url_email_uniqness
  validates :name, presence: true, length: { in: 1..50 }
  validates :url, presence: true, length: { in: 1..50 }
  validates :admin_first_name, presence: true, length: { in: 1..50 }
  validates :admin_last_name, presence: true, length: { in: 1..50 }
  validates :email, presence: true, length: { in: 1..50 }
  validates :encrypted_password, presence: true
  validates :password, confirmation: true, presence: true, unless: :encrypted_password

  def url_email_uniqness
    if !self.url.blank? && Company.where(url: self.url).count>0
      errors.add(:url, 'Ya existe empresa con esa url')
    end
    unless(!self.email.blank? && Company.where(email: self.email).first.blank?)
      errors.add(:email, 'Email ya registrado')

    end
  end

  attr_accessor :password_confirmation
  

  def password
    @password ||= Password.new(self.encrypted_password) if self.encrypted_password
  end

  def password=(new_password)
    unless new_password.blank?
      @password = Password.create(new_password)
      self.encrypted_password = @password
    end
  end

end
