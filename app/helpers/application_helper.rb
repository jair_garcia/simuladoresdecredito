module ApplicationHelper
  def error_span(attribute, options = {})
    options[:class] ||= 'field_with_errors'

    content_tag(
      :span, errors_for(attribute),
      :class => options[:class]
    ) if errors_on?(attribute)
  end

  def errors_on?(attribute)
    @object.errors[attribute].present? if @object.respond_to?(:errors)
  end

  def errors_for(attribute)
    @object.errors[attribute].try(:join, ', ')
  end
end
