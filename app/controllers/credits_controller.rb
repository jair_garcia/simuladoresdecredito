class CreditsController < ApplicationController
  before_action :set_model, only: [ :edit, :update, :destroy]
  before_action :authenticate_admin!, only: [:index, :edit]
  skip_before_action :verify_authenticity_token
  helper_method :get_credit_lines

  respond_to :html

  def new
    @company = Company.where(url: params[:url]).first
    @object = Credit.new
    @object.url = @company.url
  end

  def edit
    
  end

  def create
    @object= Credit.new(get_params)
    if @object.save
      session[:credit_id]=@object.id
      redirect_to credit_path(1)
    else
      render 'new'
    end
  end

  def show
    @object = Credit.find(session[:credit_id])
  end

  def index
    @count=Credit.where(url: current_company_url).count

    unless params[:next]
      session.delete(:credit_pagination_milestones)
      @collection = Credit.where(url: current_company_url).limit(50)
    else
      session[:credit_pagination_milestones]||=[]
      session[:credit_pagination_milestones]<<  params[:next] unless(session[:credit_pagination_milestones].include?(params[:next]))
      milestone=Credit.find(params[:next])
      @collection = Credit.where(url: current_company_url).start(milestone).limit(50)
    end
  end

  protected
  def get_credit_lines
    @credit_lines||=CreditLine.where(url: @object.url).where(state: 'active').all
  end

  def get_params
    params.require(:credit).permit(:identification,:amount, 
        :months, :credit_line_id,:url ).update({birthday: 
          Date.new(params.require(:credit)["birthday(1i)"].to_i,
            params.require(:credit)["birthday(2i)"].to_i,
            params.require(:credit)["birthday(3i)"].to_i)})

    end

  def set_model
    @object = Credit.find(params[:id])
  end
end
