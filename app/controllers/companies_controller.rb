class CompaniesController < ApplicationController
  respond_to :html
	def new
		@object = Company.new
	end

  def create
    @object = Company.new(get_params)
    if @object.save
      session[:company_session]=@object.url
      session[:company_email]=@object.email
      flash[:notice]='Ha iniciado sesion'
      redirect_to credit_lines_path 
    else
      render 'new'
    end
  end
  protected

  def get_params
    params.require('company').permit(%w{url name 
    	email admin_first_name admin_last_name password password_confirmation})
  end

end
