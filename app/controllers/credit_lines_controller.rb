class CreditLinesController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_model, only: [:show, :edit, :update, :destroy]
  respond_to :html

  def index
    @collection = CreditLine.where(url: current_company_url).where(state: 'active').all
  end

  def show
    redirect_to credit_lines_path
  end
  
  def new
    @object = CreditLine.new
    respond_with(@object)
  end

  def edit
    respond_with(@object)
  end

  def create
    @object= CreditLine.new(get_params)
    @object.url = current_company_url
    @object.save
    respond_with(@object)
  end

  def update
    @object.attributes=@object.attributes.update(get_params.symbolize_keys)
    if @object.save
      redirect_to credit_lines_path
    else
      render 'edit'
    end
  end

  def destroy
   # @object.destroy
    #No elimina, solo desactiva
    
    @object.state = 'disabled'
    @object.save
    respond_with(@object, :location=> credit_lines_path)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_model
      @object = CreditLine.find(params[:id], :consistent_read => true)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def get_params
      params.require(:credit_line).permit(:name, :anual_rate)
    end

    def get_fields
      %w{name anual_rate}
    end
end

