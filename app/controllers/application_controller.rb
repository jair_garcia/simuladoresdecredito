class ApplicationController < ActionController::Base
  responders :flash
  helper_method :current_admin_email, :current_company_url, :admin_signed_in?, :get_fields

  protected
  def get_fields
    %w{name}
  end

  def current_admin_email
    session[:company_email]
  end
  def current_company_url
    session[:company_session]
  end

  def admin_signed_in?
    !session[:company_session].blank?
  end

  def authenticate_admin!
    unless admin_signed_in?
      session[:return_url]=request.url
      redirect_to new_company_session_path
    end
  end
end
