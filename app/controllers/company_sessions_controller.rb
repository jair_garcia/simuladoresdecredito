class CompanySessionsController < ApplicationController
	respond_to :html
	def new
	end

  def create
    company=Company.where(email: get_params[:email]).all.first
    if company && company.password == get_params[:password]
    	session[:company_session]=company.url
    	session[:company_email]=company.email
    	flash[:notice]='Ha iniciado sesion'
    	redirect_url = credit_lines_path
    	if session[:return_url]
    		redirect_url = session[:return_url]
    		session[:return_url]=nil
    	end
    	redirect_to redirect_url
    else
    	flash[:notice]='Usuario o contraseña incorrectos'
    	redirect_to new_company_session_path
    end

  end

  def destroy
  	session[:company_session]=nil
    session[:company_email]=nil
    session.destroy
    flash.now[:notice]='Se ha cerrado sesión'
    render 'new'
  end
  protected

  def get_params
    @params||=params.require('session').permit(%w{email password})
    
  end
end
