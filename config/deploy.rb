require 'bundler/capistrano'
#require 'sushi/ssh'
load 'deploy/assets'

set :application, "simuladoresdecredito"
#set :repository,  "git@bitbucket.org:jair_garcia/simuladoresdecredito.git"
set :repository, '.'
set :gateway, "jair@perses.uniandes.edu.co"
set :use_sudo, false
set :deploy_via, :copy

require "rvm/capistrano"

set :rvm_ruby_string, 'ruby-2.1.2'
set :rvm_type, :user 

set :scm, :none # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

role :web, "estudiante@172.24.99.181"                          # Your HTTP server, Apache/etc
role :app, "estudiante@172.24.99.181"                          # This may be the same as your `Web` server
role :db,  "estudiante@172.24.99.195", :primary => true # This is where Rails migrations will run
#role :db,  "your slave db-server here"

set :ssh_options, { :forward_agent => true }
set :deploy_to, "/home/estudiante/simuladoresdecredito"

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
 namespace :deploy do
   task :start do ; end
   task :stop do ; end
   task :restart, :roles => :app, :except => { :no_release => true } do
     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
   end
 end