#!/bin/bash
export timestamp=$(date +%Y%m%d%H%M%S )
export app=simuladoresdecredito
export linked_dirs="bin log tmp vendor/bundle"
export bundle_path=~/.rvm/bin/ruby2_bundle

echo cd  ~/$app/shared/cache
cd  ~/$app/shared/cache; 
echo git pull origin production
git pull origin production
mkdir -p ~/$app/releases/$timestamp
tar -cvvf /tmp/$app_$timestamp.tar -C ~/$app/shared/cache/ .
#cd 
tar -xvvf /tmp/$app_$timestamp.tar -C ~/$app/releases/$timestamp/

for ldir in $linked_dirs ; do
        echo linking $ldir
        echo mkdir -p ~/$app/shared/$ldir
        mkdir -p ~/$app/shared/$ldir
        echo rm -rf ~/$app/releases/$timestamp/$ldir
        rm -rf ~/$app/releases/$timestamp/$ldir
        ln -s ~/$app/shared/$ldir ~/$app/releases/$timestamp/$ldir
        echo ln -s ~/$app/shared/$ldir ~/$app/releases/$timestamp/$ldir
done
echo $bundle_path install --binstubs ~/$app/shared/bin --path ~/$app/shared/bundle --without development test --deployment --quiet
$bundle_path install --binstubs ~/$app/shared/bin --path ~/$app/shared/bundle --without development test --deployment --quiet
echo rm -rf ~/$app/current
rm -rf ~/$app/current
echo ln -s ~/$app/releases/$timestamp ~/$app/current
ln -s ~/$app/releases/$timestamp ~/$app/current
cd ~/$app/current
#Start command for workers
/usr/bin/nohup /home/ubuntu/.rvm/bin/ruby2_bundle exec rails runner CreditQueue.process_credits  -e production &
#Start command for web
/home/ubuntu/.rvm/bin/ruby2_bundle exec rake assets:precompile