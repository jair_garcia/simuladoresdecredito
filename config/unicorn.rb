listen "127.0.0.1:8080" 
worker_processes 4
user 'estudiante'
working_directory "/home/estudiante/simuladoresdecredito/current"
pid "/home/estudiante/simuladoresdecredito/shared/pids/unicorn.pid"
stderr_path "/home/estudiante/simuladoresdecredito/shared/log/unicorn.log"
stdout_path "/home/estudiante/simuladoresdecredito/shared/log/unicorn.log"
