HireFire::Resource.configure do |config|
  config.dyno(:worker) do
    CreditQueue.get_queue.size
  end
end