# Be sure to restart your server when you modify this file.

if Rails.env == 'development' 
	Rails.application.config.session_store :cookie_store, key: '_SimuladoresDeCredito_session'
else
	Rails.application.config.session_store(ActionDispatch::Session::CacheStore, :expire_after => 20.minutes)
end
