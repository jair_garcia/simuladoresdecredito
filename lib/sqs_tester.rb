1.upto(1) do |index|
	thr = Thread.new do 
		quantity=((index*24.0)/60.0).ceil
		c=CreditLine.where(url: 'csh').all.last
		row=[]
		row << index
		row << DateTime.now
		row << quantity
		row << Credit.count
		row <<  CreditQueue.get_queue.approximate_number_of_messages
		File.open(File.join(Rails.root, 'sqs_report.csv'), 'a') do |f| 
			f.puts(row.map{|a| a.to_s.gsub(',',':')}.join(","))
		end
		ActiveRecord::Base.logger.warn("SSend #{index}")
		1.upto(quantity) do |i| 
		  Credit.create!(identification: "#{index} #{i}", credit_line_id: c.id, 
		  	birthday: 10.years.ago, amount: 10000, months:12, url: 'csh') ; 
			ActiveRecord::Base.logger.warn("SSent #{index} #{i}")
		end
	end
	sleep 60
end