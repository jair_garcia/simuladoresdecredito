require 'bcrypt'
class Company
  include Dynamoid::Document
  table :key => :url

  field :name
  field :url
  field :admin_first_name
  field :admin_last_name
  field :email
  field :encrypted_password
  field :created_at, :datetime
  has_many :credit_lines

  validate :url_email_uniqness
  validates :name, presence: true, length: { in: 1..50 }
  validates :url, presence: true, length: { in: 1..50 }
  validates :admin_first_name, presence: true, length: { in: 1..50 }
  validates :admin_last_name, presence: true, length: { in: 1..50 }
  validates :email, presence: true, length: { in: 1..50 }
  validates :encrypted_password, presence: true
  validates :password, confirmation: true, presence: true, unless: :encrypted_password

  def url_email_uniqness
    if !self.url.blank? && Company.find(self.url)
      errors.add(:url, 'Ya existe empresa con esa url')
    end
    unless(!self.email.blank? && Company.where(email: self.email).all.blank?)
      errors.add(:email, 'Email ya registrado')

    end
  end

  index :email
  attr_accessor :password_confirmation
  
  def id
    url
  end
  #security
  include BCrypt

  def password
    @password ||= Password.new(self.encrypted_password) if self.encrypted_password
  end

  def password=(new_password)
    unless new_password.blank?
      @password = Password.create(new_password)
      self.encrypted_password = @password
    end
  end
end
