require 'aws-sdk'
class CreditQueue 
  #http://docs.aws.amazon.com/AWSRubySDK/latest/AWS/SQS/Queue.html
  #http://docs.aws.amazon.com/AWSRubySDK/latest/AWS/DynamoDB.html
  def self.get_config
    @@sqs||=AWS::SQS.new
  end

  def self.get_queue
    @@queues||=get_config.queues[Settings.sqs.queue_url]
  end

  def self.send_credit(credit)
    self.get_queue.send_message(credit.id.to_s)
  end
  #Control errores 
  def self.process_credits()
    ActiveRecord::Base.logger.error("Starting")
    loop do
      puts("Procesando ...")
      ActiveRecord::Base.logger.info("Scanning")
      begin
        self.get_queue.receive_message(limit: 1, wait_time_seconds: 10) do |receive_message|  
          ActiveRecord::Base.logger.warn("Processing #{receive_message.body} ..")
          cliente=Credit.find(receive_message.body)
          cliente.generate_payment_plan
          ActiveRecord::Base.logger.info("Done #{receive_message.body}")
        end
      rescue  
        ActiveRecord::Base.logger.error($!.message)
      end
    end
  end
end