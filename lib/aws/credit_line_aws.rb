class CreditLine
  include Dynamoid::Document
  table name: 'credit_line'
  field :name
  field :anual_rate, :float
  field :url
  field :state

  index :url

  validates :name, :length=> {maximum: 50}, :presence=>true
  validates :anual_rate, presence: true, 
  	numericality: {greater_than: 0, less_than: 100}
  validates :url, presence: true

  before_create :set_state

  def set_state
    self.state = 'active'
  end

	def company
		@company||=Company.where(url: self.url).first
	end
end