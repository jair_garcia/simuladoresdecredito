# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140827182716) do

  create_table "admins", force: true do |t|
    t.string   "nombres"
    t.string   "apellidos"
    t.string   "email"
    t.integer  "empresa_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true

  create_table "clientes", force: true do |t|
    t.string   "identificacion"
    t.date     "fecha_nacimiento"
    t.integer  "valor_credito"
    t.integer  "plazo_meses"
    t.integer  "linea_credito_id"
    t.integer  "empresa_id"
    t.string   "estado",           default: "esperando"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cuota", force: true do |t|
    t.integer  "numero"
    t.decimal  "valor_cuota"
    t.decimal  "amortizacion"
    t.decimal  "valor_intereses"
    t.decimal  "saldo"
    t.integer  "plan_pago_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "empresas", force: true do |t|
    t.string   "nombre"
    t.string   "url"
    t.integer  "linea_credito_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "linea_creditos", force: true do |t|
    t.string   "nombre"
    t.decimal  "tasa_ea"
    t.string   "estado",     default: "activo"
    t.integer  "empresa_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "plan_pagos", force: true do |t|
    t.decimal  "valor_cuota"
    t.decimal  "riesgo"
    t.integer  "cliente_id"
    t.decimal  "valor_pagado"
    t.decimal  "valor_intereses"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
